#!/usr/bin/python
import cgitb
import cgi
cgitb.enable()
print("Content-Type: text/html")
print()
import json,urllib.request
import requests

form = cgi.FieldStorage()
containers = form.getvalue('container')
url = form.getvalue('url')
port = form.getvalue('port')
join = url + ":" + port

start = "http://" + join + "/containers/" + containers  + "/start"
requests.post(start)
