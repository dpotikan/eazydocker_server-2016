#!/usr/bin/python
import cgitb
import time
import datetime
import pprint
cgitb.enable()

# Print necessary headers.
print("Content-Type: text/html")
print()

import pymysql
import json
import requests
import urllib.request

host = 'localhost'
user = 'root'
passwd = 'Dom546275'
db = 'docker_db'
url = []
stat = []
sum_cpu = 0
sum_mem_usage = 0
sum_mem_limit = 0
sum_net_i = 0
sum_net_o = 0
sum_blk_i = 0
sum_blk_o = 0
sum_mem_per = 0

conn = pymysql.connect(host,user,passwd,db)
cur = conn.cursor()

query = "SELECT * FROM registry_db"
cur.execute(query)

for rows in cur:
    ip_addr = rows[1]
    port = rows[2]
    join = ip_addr + ":" + port
    url.append(join)

for count in range(0, len(url)):
    hostURL = 'http://' + url[count] + '/containers/json'
    try:
        request = urllib.request.urlopen(hostURL, None, 1)
    except:
        request = None
        continue
    data = json.loads((request.read().decode("utf-8")))

    for index in range(0, len(data)):
        id = data[index]['Id']
        name = data[index]['Names']
        name = name[0][1:len(name[0])]

        image = data[index]['Image']

name = name[0][1:len(name[0])]

image = data[index]['Image']
hostStat = 'http://' + url[count] + '/containers/' + id + '/stats?stream=false'
statlist = urllib.request.urlopen(hostStat)
status = statlist.read().decode("utf-8")
status = json.loads(status)

cpu = status['cpu_stats']['cpu_usage']['total_usage'] / 100000000000
mem_usage = status['memory_stats']['usage'] / 1000000
mem_limit = status['memory_stats']['limit'] / 1000000
net_i = status['networks']['eth0']['rx_bytes'] / 1000
net_o = status['networks']['eth0']['tx_bytes'] / 1000
if (status['blkio_stats']['io_service_bytes_recursive'] is None or len(
        status['blkio_stats']['io_service_bytes_recursive']) == 0):
    blk_i = 0
    blk_o = 0
else:
    blk_i = status['blkio_stats']['io_service_bytes_recursive'][0]['value'] / 1000000
    blk_o = status['blkio_stats']['io_service_bytes_recursive'][1]['value']

if (cpu is None):
    cpe = 0
if (mem_usage is None):
    if (mem_usage is None):
        mem_usage = 0
    if (mem_limit is None):
        mem_limit = 0
    if (net_i is None):
        net_i = 0
    if (net_o is None):
        net_o = 0
    if (blk_i is None):
        blk_i = 0
    if (blk_o is None):
        blk_o = 0

    if (mem_limit == 0):
        mem_per = 0
    else:
        mem_per = (mem_usage / mem_limit) * 100

    sum_cpu = (sum_cpu + cpu)
    sum_mem_usage = sum_mem_usage + mem_usage
    sum_mem_limit = sum_mem_limit + mem_limit
    sum_net_i = sum_net_i + net_i
    sum_net_o = sum_net_o + net_o
    sum_blk_i = sum_blk_i + blk_i
    sum_blk_o = sum_blk_o + blk_o
    sum_mem_per = sum_mem_per + mem_per

    sql2 = """INSERT INTO usage_db(c_name,date,cpu_per,m_usage,m_limit,m_per,net_i,net_o,blk_i,blk_o,h_url)
        VALUES('%s',%d,%f,%f,%f,%f,%f,%f,%f,%f,'%s')""" % (
    name, int(time.time() + 25200), cpu, mem_usage, mem_limit, mem_per, net_i, net_o, blk_i, blk_o, url[count])
    cur.execute(sql2)
    conn.commit()

sql3 = """INSERT INTO resource_usage_db(date,cpu_per,m_usage,m_limit,m_per,net_i,net_o,blk_i,blk_o,h_url)
VALUES(%d,%f,%f,%f,%f,%f,%f,%f,%f,'%s')""" % (
time.time() + 25200, sum_cpu, sum_mem_usage, mem_limit, sum_mem_per, sum_net_i, sum_net_o, sum_blk_i, sum_blk_o,
url[count])
cur.execute(sql3)
conn.commit()

sum_cpu = 0
sum_mem_usage = 0
sum_mem_limit = 0
sum_net_i = 0
sum_net_o = 0
sum_blk_i = 0
sum_blk_o = 0
sum_mem_per = 0
#       cpu = data[index]['precpu_stats']['cpu_usage']['total_usage']
#       stat = urllib.request.urlopen(("http://" + url[count] + "/containers/" + id + "/stats"),None,1)
#       print("id : " ,id, "name : " , name, "image : " , image, "cpu : ",cpu)

