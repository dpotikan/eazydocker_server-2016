#!/usr/bin/python
import pymysql
import cgitb
import cgi
import json
import time
import datetime
import json
import operator

cgitb.enable()
print("Content-Type: text/html")
print()
session = []
array = []
index = []
summary = []
db = pymysql.connect(host="localhost",
                     user="root",
                     passwd="Dom546275",
                     db="docker_db")
cur = db.cursor()
form = cgi.FieldStorage()
url = form.getvalue('url')
port = form.getvalue('port')
join = url + ":" + port
findDate = ("SELECT date FROM usage_db WHERE h_url like '%s' ORDER BY date DESC LIMIT 1") %join
cur.execute(findDate)
row = cur.fetchone()
date1 = row[0]
starter = int(date1)-43200
select = ("SELECT c_name,date,cpu_per,m_per FROM usage_db WHERE date > %d and h_url like '%s'"%(starter,join))
cur.execute(select)
numrows = int(cur.rowcount)

cpuDefaultTheshold = 0.5
memDefaultTheshold = 0.5
for x in range(0,numrows):
        row = cur.fetchone()
        cpu_per = row[2]
        m_per = row[3]
        date = row[1]
        nowdate = str(datetime.datetime.fromtimestamp(int(date)))
        c_name = row[0]
        theshold = "SELECT set_cpu,set_mem FROM theshold WHERE c_name like '%s' AND h_url like '%s'" %(c_name,join)
        cur2 = db.cursor()
        cur2.execute(theshold)
        set = cur2.fetchone()
        if(set is None):
                peakCPU = cpuDefaultTheshold
                memPEAK = memDefaultTheshold
        else:
                if(set[0] is None and set[1] is not None):
                        peakCPU = cpuDefaultTheshold
                if(set[1] is None and set[0] is not None):
                        memPEAK = memDefaultTheshold
                else:
                    peakCPU = set[0]
                    memPEAK = set[1]
                if (float(cpu_per) > peakCPU or float(m_per) > memPEAK):
                    if (float(cpu_per) > peakCPU and float(m_per) < memPEAK):
                        array.append({'Date': nowdate, 'Cpu_per': cpu_per, 'C_name': c_name})
                        index.append(date)
                    if (float(cpu_per) < peakCPU and float(m_per) > memPEAK):
                        array.append({'Date': nowdate, 'M_per': m_per, 'C_name': c_name})
                        index.append(date)
                    if (float(cpu_per) > peakCPU and float(m_per) > memPEAK):
                        array.append({'Date': nowdate, 'Cpu_per': cpu_per, 'M_per': m_per, 'C_name': c_name})
                        index.append(date)

data = sorted(array, key=operator.itemgetter('Date'))
data.reverse()
index.reverse()
i = 0
while i < len(index):
    if ((len(index) - i) >= 6):
        default = index[i] - 1800
        if(index[i+1]> default and index[i+2]> default and index[i+3]> default and index[i+4]> default):
            summary.append(data[i])
        i = i+6
    else:
        i = i+6


data2 = json.dumps({'Data':summary ,'Status':True})

print(data2)

db.close()
