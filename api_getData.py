#!/usr/bin/python
import pymysql
import cgitb
import cgi
import json
import time
import datetime
cgitb.enable()
print("Content-Type: text/html")
print()
session = []
array = []

db = pymysql.connect(host="localhost",
                     user="root",
                     passwd="Dom546275",
                     db="docker_db")
cur = db.cursor()
findDate = ("SELECT date FROM resource_usage_db ORDER BY date DESC LIMIT 1")
cur.execute(findDate)
row = cur.fetchone()
date = row[0]
starter = int(date)-86400
select = ("SELECT date,cpu_per,m_per,net_o FROM resource_usage_db WHERE date > %d"%(starter))
cur.execute(select)
numrows = int(cur.rowcount)
for x in range(0,numrows):
    row = cur.fetchone()
    date = row[0]
    nowdate = str(datetime.datetime.fromtimestamp(int(date)))
    cpu_per = row[1]
    m_per = row[2]
    net_o = row[3]
    array.append({'Date': nowdate, 'Cpu_per': cpu_per,'M_per':m_per,'Net_o':net_o})
data = json.dumps({'Data':array ,'status':True})
print(data)

db.close()
